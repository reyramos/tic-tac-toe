/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'ApplicationController',
	[
		 '$scope'
		, '$log'
		, 'socketService'
	     , 'enumsService'
		, function
		(
			 $scope
			, $log
			, socketService
			, enumsService
			) {

		$scope.onOPenMessage = "Pending Player";
		$scope.hideModal = false;

		$scope.selectBox = function(id){
			$log.log(id)
			$('#box'+id ).addClass('active')
			socketService.send({move:id} ).then(function(data){

			})

		}


		$scope.$on ( enumsService.socket.open, function () {
			$log.log(enumsService.socket.open)

			socketService.send({message:'Hello World'}).then(function(result){
				$log.log('ApplicationController >> socketService.send >> result',result)

			})
		} );

		$scope.$on ( enumsService.socket.close, function () {
			$log.log(enumsService.socket.close)

		});

	}]
)
