

angular.module('app').factory
('enumsService'
	, function () {
	 var enums = {
		 site: {
			 name: 'Socket'
			 , reset: 6
		 },
		 socket:{
			 open:  '===================== Socket Connection Open  ===================='
			 , close: '===================== Socket Connection Close ===================='
			 , key:'d8a9f4dab692900dc7b595468d1e36f0'
			 , url : "192.168.1.17"
			 , port:9999
		 },
		 spinnerMessages:{
			 loading : 'Loading'
			 , lostConnection : 'Lost Connection'
		 },
		 status:{
			 FAILED: 'FAILED'
			 , OK: 'OK'
		 }
	 }
	 return enums
 }
)