/**
 * # SockJS socket management service
 *
 * Creates SockJS socket connection to server, re-connects on disconnection,
 * and exports hooks to map handlers for various data interactions.
 *
 */
angular.module ( 'app' ).factory ( 'socketService', [
	'$q'
	, '$rootScope'
	, '$log'
	, 'enumsService'
	, function (
		  $q
		, $rootScope
		, $log
		, enumsService
		) {

		var createSocket;
		createSocket = function () {

//			var socket = new WebSocket ( 'ws://' + enumsService.socket.url + ':' + enumsService.socket.port );

			var socket = new SockJS	( '//' + document.domain + ':' + enumsService.socket.port + '/api/socket'
				, ''
				, { 'debug':true
					, 'devel' : true
					, 'protocols_whitelist':
						[ 'xdr-streaming'
							, 'xdr-polling'
							, 'xhr-streaming'
							, 'iframe-eventsource'
							, 'iframe-htmlfile'
							, 'xhr-polling'
							, 'websocket'
						]
				}
			);


			socket.onopen = function () {
				service.open = true;
				service.times_opened++;

				$rootScope.$broadcast ( enumsService.socket.open );

				var args = arguments;

			}

			socket.onmessage = function ( data ) {

				var args = arguments;
				try{
					args = JSON.parse ( data.data );
				}catch ( e ){

				}
				$rootScope.$apply(
					function(){
						service.listening.apply(args, socket)
					}
				)
			}

			socket.onclose = function () {
				var args = arguments;
				service.open = false;

				$rootScope.$broadcast ( enumsService.socket.close );
				$rootScope.$apply(
					function(){
						service.onclose.apply(args, socket)
					}
				)
			}

			return socket;
		};
		//start socketConnection
		var socket = createSocket ();

		// Create a unique callback ID to map requests to responses
		var currentCallbackId = 0;

		// This creates a new callback ID for a request
		function getCallbackId () {
			currentCallbackId += 1;
			//reset callback id
			if ( currentCallbackId > 10000 ){
				currentCallbackId = 0;
			}
			return currentCallbackId;
		}

		var service = {
			  callbacks:{} // Keep all pending requests here until they get responses
			, open:false
			, times_opened:0
			, onclose:
				function(){
					setTimeout (
						function () {
						socket = createSocket ();
					}, 1000 );
				}
			, listening:function () {
				if ( service.callbacks.hasOwnProperty ( this.callback_id ) ){
					service.callbacks[this.callback_id].cb.resolve(this)
					delete service.callbacks[this.callback_id];
				}
			}
			, send:function ( data ) {
				var defer = $q.defer()
				var callbackId = getCallbackId ();
				this.callbacks[callbackId] = {
					time:new Date (),
					cb:defer
				};

				data.callback_id = callbackId;
				$log.log ( 'socketService >> sending >', data );

				var msg = typeof(data) == "object" ? JSON.stringify ( data ) : data;
					socket.send(msg)

				return defer.promise;
			}

		}

		return service;
	}] );