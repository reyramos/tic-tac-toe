var http = require('http');
var sockjs = require('sockjs');

var socket = sockjs.createServer();
var sockets = [];

socket.on('connection', function(conn) {
	conn.on('data', function(message) {

		data = JSON.parse ( message )

		getSourceIp( conn );

		//keep a record of the instance
		sockets.push( conn );

		var data = {
			message:"Welcome from Node Socket",
			callback_id: data.callback_id
		}
		console.log(message)


		msg = JSON.stringify ( data)
		//echo the message
		conn.write(msg);


	});
	conn.on('close', function() {


	});
});


var server = http.createServer();
socket.installHandlers(server, {prefix:'/api/socket'});
server.listen(9999, '0.0.0.0');



function getSourceIp( conn ) {
	if (
		typeof conn.headers !== 'undefined'
			&& typeof conn.headers['x-forwarded-for'] !== 'undefined'
			&& conn.headers['x-forwarded-for'] !== ''
		) {
		conn._sourceIp = conn.headers['x-forwarded-for'];
		conn._sourceIpVar = 'xff';
	} else if (
		typeof conn.remoteAddress !== 'undefined'
			&& conn.remoteAddress!== ''
		) {
		conn._sourceIp = conn.remoteAddress;
		conn._sourceIpVar = 'ra';
	}
}